Cette application est conçu Python, Flask, Bootstrap et fontawesome.

l'application est organisé en dossier :

- template contient tous nous fichier html
- forms.py permet la sécurité des formulaires de notre application:
  - le formulaire d'enregistrement
  - le formulaire de login
- models.py : contient les ORM de notre base de donnée.
  ORM permet:
  - de créer les tables de notre base de donnée
  - de communiquer avec la base de donnée
  - de modifier les informations de la base de donnée
- routes.py contient toutes les route (views) de notre blogue
- utilisation d'une base de donnée SQLALCHEMY

fonctionnalité:

L'application permet :

- de s'enregistrer et de générer des erreurs liées à l'enregistrement, le nom d'utilisateur et le E-mail ne doivent pas se répété dans la base de données
- de se connecter apres inscription
- de ne pas voir certaines pages sans etre connecté
- permet de ne pas voir les pages 'register' et 'login' etant connecté
- aux utilisateurs de publier des articles
- aux utilisateurs de modifier, de supprimer les posts qu'ils ont publié
- aux utlisateurs de ne pas modifier ni supprimer les posts qu'ils n'ont pas publié
- aux utilisateurs de faire la mise a jour de leur informations personnelles
- aux utilisateurs de reinitialiser leur mot de passe en toute sécurité a traves un lien de réinitialisation envoyé dans leur email
- la mise en évidense des erreurs (404, 403, 500)
