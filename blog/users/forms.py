from flask_wtf import FlaskForm
from flask_login import current_user
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, PasswordField, TextAreaField, SubmitField,  BooleanField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError

from blog.models import User





class RegistrationForm(FlaskForm):
    username = StringField('Nom d\'utilisateur * :', validators=[DataRequired(message ="Le nom d'utilisateur est obligatoire")])
    email = StringField('E-mail * :', validators=[DataRequired(message ="L'adresse E-mail est obligatoire"), Email(message ="Adresse E-mail invalide")])
    password = PasswordField('Mot de passe * :', validators=[DataRequired(message ="Le mot de passe est obligatoire"), Length(min=8, max=25, message = "Le mot de passe doit être compris en 8 - 25 carractères !!!")])
    password_confir = PasswordField('Confirmation du mot de passe * :', validators=[DataRequired(message ="La confirmation du mot de passe est obligatoire"), EqualTo('password', message ="Mot de passe non conforme !!!")])
    submit =  SubmitField("S'enregister")

    #Controleur du formulaire vérification du username
    def validate_username(form, username):
        if User.query.filter_by(username=username.data).first():
            raise ValidationError('Ce username existe déja !!!')
    
    #Controleur du formulaire vérification de l'adresse email
    def validate_email(form, email):
        if User.query.filter_by(email=email.data).first():
            raise ValidationError('Cette adresse email existe déja !!!')

#Formulaire de mise a jour des informations du user apres inscription
class UpdateAccountForm(FlaskForm):
    username = StringField('Nom d\'utilisateur :', validators=[DataRequired(message ="Le nom d'utilisateur est obligatoire")])
    email = StringField('E-mail :', validators=[DataRequired(message ="L'adresse E-mail est obligatoire"), Email(message ="Adresse E-mail invalide")])
    picture = FileField('Changer la photo de profil :', validators=[FileAllowed(['jpg', 'jpeg', 'png', 'gif'], message="Type de fichier non pris en compte")] )

    submit =  SubmitField("Mise à jour")

    def validate_username(form, username):
        if username.data != current_user.username:
                if User.query.filter_by(username=username.data).first():
                    raise ValidationError('Ce username existe déja !!!')
    
    def validate_email(form, email):
        if email.data != current_user.email:
            if User.query.filter_by(email=email.data).first():
                raise ValidationError('Cette adresse email existe déja !!!')



class LoginForm(FlaskForm):
    email = StringField('E-mail * :', validators=[DataRequired(message ="L'adresse E-mail est obligatoire"), Email(message ="Adresse E-mail invalide")])
    password = PasswordField('Mot de passe * :', validators=[DataRequired(message ="Le mot de passe est obligatoire"), Length(min=2, max=25, message = "Le mot de passe doit avoir en 2 - 8 carractères !!!")])

    remember =  BooleanField('Remember Me')
    submit =  SubmitField("Se connecter")


#formulaire pour vérifier le email
class RequestResetForm(FlaskForm):
    email = StringField('E-mail * :', validators=[DataRequired(message ="L'adresse E-mail est obligatoire"), Email(message ="Adresse E-mail invalide")])
    submit =  SubmitField("Recevoir le lien")

    #  Vérification si l'email est dans la base de donnée
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError('Cette adresse email n\'exsiste pas')

#formulaire pour réinitialiser le mot de passe
class ResetPasswordForm(FlaskForm):
    password = PasswordField('Nouveau password * :', validators=[DataRequired(message ="Le mot de passe est obligatoire"), Length(min=8, max=25, message = "Le mot de passe doit avoir en 8 - 28 carractères !!!")])
    password_confir = PasswordField('Confirm password * :', validators=[DataRequired(message ="La confirmation du mot de passe est obligatoire"), EqualTo('password', message ="Mot de passe non conforme !!!")])
    submit =  SubmitField("Réinitialiser le mot de passe")