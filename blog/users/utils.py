#Utils.py va contenir toutes nos fonctions relatives a l'application user

from PIL import Image
import secrets, os
from flask_mail import Message
from blog import app, mail
from flask import url_for

from datetime import datetime

#fonction permetant de sauvégarder les images en les renommant
def save_picture(form_picture):
    random_hex = secrets.token_hex(11) #création de code hexdécimal à 11 caractères
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/img', picture_fn)

    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


#Fonction d'envoie du token dans le email
def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Réinitialison de votre mot de passe', recipients=[user.email])
    msg.body = f''' 
    Bonjour {user.username}, si vous aviez demandé un lien pour réinitialiser votre mot de passe liquez sur ce lien ci-dessous, dans le cas contraire ignorez ce message.
    \nCe lien est valable que 2 mn :
    \n{url_for('users.reset_token', token = token, _external = True)}'''
    mail.send(msg)


# fonction Context Processors pour trouver le pluriel de post
@app.context_processor
def utility_processor():
    def pluriels(cunt, singulier, pluriel=None):
        if not isinstance(cunt, int):
            raise ValueError('{} n\'est pas un nombre entier'.format(cunt))
        if pluriel is None:
            pluriel = singulier + 's'

        if cunt == 1:
            resultat = singulier
        else:
            resultat = pluriel
        return "{} {}".format(cunt, resultat)
    return dict(pluriels=pluriels)


# fonction Context Processors pour avoir la date et l'heure 
@app.context_processor
def date_now():
    return dict(now = datetime.now())