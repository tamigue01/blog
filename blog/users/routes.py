#users/route.py va contenir toutes les route relatives a l'application users


from flask import render_template, url_for, flash, redirect, session, abort, request, Blueprint

from blog import bcrypt, db, mail
from blog.users.forms import RegistrationForm, LoginForm, UpdateAccountForm, ResetPasswordForm, RequestResetForm
from blog.models import User, Post



#pour controler certaine routes (rendre des routes visible qu'à la connexion de l'utilisateur)
from flask_login import login_user, current_user, login_required, logout_user

from flask_mail import Message 
from blog.users.utils import save_picture, send_reset_email

users = Blueprint('users', __name__)




@users.route('/register', methods=['GET', 'POST'])
def register():
    
    #Controleur si un utilisateur est deja connecté il ne peut plus s'enregister 
    if current_user.is_authenticated:
        flash("Vous aviez déja un compte", "danger")
        return redirect(url_for("main.home"))

    form = RegistrationForm()
    if form.validate_on_submit():
        pw_hash = bcrypt.generate_password_hash(form.password.data) #cryptage du mot de passe du user
        user = User(
            username = form.username.data,
            email = form.email.data,
            password = pw_hash
        )
        db.session.add(user)
        db.session.commit()
        
        flash("Votre compte à été créé avec succès, veuillez vous connecter", 'success')
        return redirect(url_for('users.login'))
    return render_template('register.html', title="s'enrégistrer", form = form)


@users.route('/login', methods=['GET', 'POST'])
def login():

    #Controleur si un utilisateur est deja connecté il ne peut plus ce connecter a nouveau
    #il faut qu'il se deconnecte d'abord
    if current_user.is_authenticated:
        flash("Vous êtes déjà connecté ", "danger")
        return redirect(url_for("main.home"))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            if bcrypt.check_password_hash(user.password, form.password.data):
                login_user(user, remember=form.remember.data)
                flash("Bienvenue {}".format(user.username), 'success')
              
                return redirect (request.args.get("next") or url_for("main.home"))
            else:
                flash("Mot de passe incorrect, veuillez reessayer", 'danger')
        else:
            flash("E-mail incorrect, veuillez reessayer", 'danger')
    return render_template('login.html',form = form, title="se connecter")


@users.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("users.login"))


@users.route("/account/update", methods = ["GET", "POST"])
@login_required
def account_update():
    form = UpdateAccountForm()
    if form.validate_on_submit():

        #la condition pour la modification de l'image d'utilisation
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file

        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash("Mise a jour de votre profil effectuée avec succès", "success")
        return redirect(url_for("users.account"))
    
    elif request.method == "GET":
        form.username.data = current_user.username
        form.email.data = current_user.email

    # le schemin ou le dossier contenant les images 
    image = url_for('static', filename='img/' + current_user.image_file)
    return render_template('account_update.html', title = "Mise à jour de mon profil", image=image, form=form)

@users.route("/account", methods = ["GET", "POST"])
@login_required
def account():
    # le schemin ou le dossier contenant les images 
    image = url_for('static', filename='img/' + current_user.image_file)
    return render_template('account.html', title = "Mon profil", image=image)


#Pour voir tous les posts d'un utilisateur
@users.route('/user/<string:username>')
def user_posts(username):
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.filter_by(author=user).order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
    return render_template('user_posts.html', posts=posts, user=user)

#pour réinitialiser le mot de passe
@users.route("/reset_password", methods = ["POST", "GET"])
def reset_request():
    #Controleur si un utilisateur est deja connecté il ne peut plus ce connecter a nouveau
    #il faut qu'il se deconnecte d'abord
    if current_user.is_authenticated:
        flash("Vous êtes déjà connecté ", "danger")
        return redirect(url_for("main.home"))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash("Le lien de réinitialisation à été envoyé dans votre email", "success")
        return redirect(url_for('users.login'))

    return render_template('reset_request.html', form = form , title = 'Réinitialison du mot de passe')

@users.route("/reset_password/<token>", methods = ["POST", "GET"])
def reset_token(token):
    #Controleur si un utilisateur est deja connecté il ne peut plus ce connecter a nouveau
    #il faut qu'il se deconnecte d'abord
    if current_user.is_authenticated:
        flash("Vous êtes déjà connecté ", "danger")
        return redirect(url_for("main.home"))

    user = User.verify_reset_token(token)
    if user is None:
        flash("Votre token est expiré", "warning")
        return redirect(url_for('users.reset_request'))

    form = ResetPasswordForm()
    if form.validate_on_submit():
        pw_hash = bcrypt.generate_password_hash(form.password.data) #cryptage du mot de passe du user
        user.password = pw_hash
        db.session.commit()
        flash("Mise à jour du mot de passe avec succès, veuillez vous connecter", 'success')
        return redirect(url_for('users.login'))
    return render_template('reset_token.html', form = form , title = 'Réinitialison du mot de passe')