from flask import Flask
from flask_sqlalchemy import SQLAlchemy

#Utilisation de bcrypt pour crypter des information tel que le mot de passe des utilisateurs
from flask_bcrypt import Bcrypt

#flask_login pour permettre la connexion de l'utilisateur
from flask_login import LoginManager

#Flask-mail pour l'envoie des mail
from flask_mail import Mail
from blog.config import Config

app = Flask(__name__)



db = SQLAlchemy(app)
app.config.from_object(Config)
bcrypt = Bcrypt(app)
mail = Mail(app)
login_manager = LoginManager(app)
login_manager.login_view = "users.login"
login_manager.login_message = u"Veuillez vous connecter pour avoir accès à cette page."
login_manager.login_message_category = "danger"



#definir les routes de notre application
from blog.users.routes import users
from blog.main.routes import main
from blog.posts.routes import posts
from blog.errors.handlers import errors

app.register_blueprint(users)
app.register_blueprint(main)
app.register_blueprint(posts)
app.register_blueprint(errors)