#posts/route.py va contenir toutes les routes relatives a l'application posts

from flask import render_template, url_for, flash, redirect, session, abort, request, Blueprint

from blog import db
from blog.posts.forms import PostForm
from blog.models import Post

#pour controler certaine routes (rendre des routes visible qu'à la connexion de l'utilisateur et e)
from flask_login import current_user, login_required


posts = Blueprint('posts', __name__)




#pour créer un nouveau post
@posts.route("/post/new", methods = ["GET", "POST"])
@login_required
def new_post():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title = form.title.data, content = form.content.data, author = current_user)
        db.session.add(post)
        db.session.commit()
        flash("Votre post à été publié ", "success")
        return redirect(url_for("posts.post", post_id=post.id))
    image = url_for('static', filename='img/' + current_user.image_file)
    return render_template('create_post.html', title = "Nouveau post", form = form, image = image)

#pour voir un post en entier
@posts.route("/post/<post_id>")
def post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.title, post=post)



#pour faire la mise a jour d'un post
#la mise a jour du post est possible si c'est le user qui a publié se post
@posts.route("/post/<post_id>/update", methods = ["GET", "POST"])
@login_required
def update_post(post_id):
    try:
        post = Post.query.get_or_404(post_id)
        form = PostForm()
        image = url_for('static', filename='img/' + current_user.image_file)

        #verification si l'auteur du post est la personne connecté
        if post.author != current_user:
            abort(403)
        if form.validate_on_submit():
            post.title = form.title.data
            post.content = form.content.data
            db.session.commit()
            flash("Post modifié avec succèss", "success")
            return redirect(url_for("posts.post", post_id=post.id))
        elif request.method == "GET":
            form.title.data = post.title
            form.content.data = post.content
        return render_template('update_post.html', title="Mise à jour du post", image = image, post=post, form = form)
    
    except AttributeError as e:
        flash("Veuillez vous connecter", "danger")
        return redirect(url_for('users.login'))

#pour supprimer un post
#la suppression du post est possible si c'est le user qui a publié se post
@posts.route("/post/<post_id>/delete", methods = ["POST"])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash("Post supprimé avec succès", "success")
    return redirect (url_for('main.home'))