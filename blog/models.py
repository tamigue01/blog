from blog import db, login_manager, app
from datetime import datetime
from flask_login import UserMixin

#permet de générer un code temporaire pour authentification pour réinitialiser notre mot de passe
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer


#fonction de récupération de l'Id de l'utilisateur a la connexion 
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

#création de la table user (utilisateur)
class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(80), unique=False, nullable=False)
    image_file = db.Column(db.String(80), unique=False, nullable=False, default = "default.jpg")

    #relation avec la table posts (un user peu avoir plusieur posts)
    posts = db.relationship('Post', backref='author', lazy=True)

    #Méthode pour générer le code pour réinitialiser le mot de passe d'un user
    def get_reset_token(self, expires_sec=120):
        s = Serializer(app.config["SECRET_KEY"], expires_sec)
        return s.dumps({"user_id":self.id}).decode("utf-8")
    
    #Méthode pour vérifier le code pour la réinitialisation
    @staticmethod
    def verify_reset_token(token):
        s = Serializer(app.config["SECRET_KEY"])
        try:
            user_id = s.loads(token)['user_id']
            return User.query.get(user_id)
        except:
            return None

    def __repr__(self):
        return "User ({}, {})".format(self.username, self.email)

#Création de la table des posts
class Post(db.Model):
    __tablename__='post'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), unique=False, nullable=False)
    content = db.Column(db.Text, unique=False, nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    #clé étrangère pour retrouver le user du post
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    

    def __repr__(self):
        return "Post ('{}', '{}')".format(self.title, self.date_posted)




#création des tables si elles n'existent pas
db.create_all()