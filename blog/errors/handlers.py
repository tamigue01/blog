#errors/handlers.py va contenir toutes les routes relatives aux erreurs de nos applications


from flask import Blueprint, url_for, request,render_template

errors = Blueprint('errors', __name__)


@errors.app_errorhandler(404)
def error_404(error):
    image = url_for('static', filename='img/er/404.jpg') 
    return render_template('errors/404.html', image=image), 404

@errors.app_errorhandler(403)
def error_403(error):
    image = url_for('static', filename='img/er/403.jpg')
    return render_template('errors/403.html', image=image), 403

@errors.app_errorhandler(500)
def error_500(error):
    image = url_for('static', filename='img/er/500.jpg')
    return render_template('errors/500.html', image=image), 500